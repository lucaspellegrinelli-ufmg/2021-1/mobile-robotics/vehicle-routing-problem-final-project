import numpy as np
from utils.math import normalize_angle

class RobotController:
  def __init__(self, sim, clientID, object_name, ax, color):
    self.sim = sim
    self.clientID = clientID
    self.object_name = object_name
    self.ax = ax
    self.color = color
    
    # Dados do robô
    self.robot_dim = (0.381, 0.0975)
    self.max_v = 0.1
    self.max_w = np.deg2rad(45)

  def initialize_robot(self):
    returnCode = 1
    while returnCode != 0:
      returnCode, robo = self.sim.simxGetObjectHandle(self.clientID, self.object_name, self.sim.simx_opmode_oneshot_wait)
      returnCode, left_motor = self.sim.simxGetObjectHandle(self.clientID, self.object_name + '_leftMotor', self.sim.simx_opmode_oneshot_wait)
      returnCode, right_motor = self.sim.simxGetObjectHandle(self.clientID, self.object_name + '_rightMotor', self.sim.simx_opmode_oneshot_wait)

    self.robot_handle = robo
    self.left_motor_handle = left_motor
    self.right_motor_handle = right_motor

    pos, _ = self.get_position_orientation()
    self.start_position = pos

  def get_position_orientation(self):
    returnCode = 1
    while returnCode != 0:
      returnCode, robo_position = self.sim.simxGetObjectPosition(self.clientID, self.robot_handle, -1, self.sim.simx_opmode_oneshot_wait)
      returnCode, robot_orientation = self.sim.simxGetObjectOrientation(self.clientID, self.robot_handle, -1, self.sim.simx_opmode_oneshot_wait)
    return robo_position, robot_orientation

  def stop(self):
    self.sim.simxSetJointTargetVelocity(self.clientID, self.left_motor_handle, 0, self.sim.simx_opmode_oneshot_wait)
    self.sim.simxSetJointTargetVelocity(self.clientID, self.right_motor_handle, 0, self.sim.simx_opmode_oneshot_wait)

  def tick(self, qgoal, backwards=False):
    pos, orient = self.get_position_orientation()

    if not backwards:
      self.ax.scatter(pos[0], pos[1], c=self.color, s=5)

    qrobot = np.array([pos[0], pos[1], orient[2]])
    dx, dy, _ = qgoal - qrobot

    rho = np.sqrt(dx**2 + dy**2) * 30
    alpha = normalize_angle(-qrobot[2] + np.arctan2(dy, dx))
    beta = normalize_angle(qgoal[2] - np.arctan2(dy, dx))

    kr = 4 / 20
    ka = 8 / 20
    kb = -1.5 / 20

    if not backwards:
      kr = -kr
      alpha = normalize_angle(alpha - np.pi)
      beta = normalize_angle(beta - np.pi)

    v = kr * rho
    w = ka * alpha + kb * beta

    # Limit v,w to +/- max
    v = max(min(v, self.max_v), -self.max_v)
    w = max(min(w, self.max_w), -self.max_w)  

    L, r = self.robot_dim
    wr = ((2 * v) + (w * L)) / (2 * r)
    wl = ((2 * v) - (w * L)) / (2 * r)

    self.sim.simxSetJointTargetVelocity(self.clientID, self.left_motor_handle, wl, self.sim.simx_opmode_oneshot_wait)
    self.sim.simxSetJointTargetVelocity(self.clientID, self.right_motor_handle, wr, self.sim.simx_opmode_oneshot_wait)