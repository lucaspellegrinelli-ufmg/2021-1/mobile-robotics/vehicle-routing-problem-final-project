import time
import numpy as np
import matplotlib.pyplot as plt
import networkx
import math
from vrpy import VehicleRoutingProblem
import logging

logging.getLogger().setLevel(logging.WARNING)

from utils.math import dist3d
from control.robot_controller import RobotController
from control.object_finder import find_all_cube_positions, find_goal_position, find_source_position

class MasterController:
  def __init__(self, sim, clientID, configs):
    self.sim = sim
    self.clientID = clientID
    self.configs = configs

    self.fig = plt.figure()
    self.ax = plt.axes()
  
  def initialize_robots(self, robot_names, colors = ["#d67e7e", "#7ed68e", "#7e98d6"]):
    self.robots = []
    for name, color in zip(robot_names, colors):
      robot = RobotController(self.sim, self.clientID, name, self.ax, color)
      robot.initialize_robot()
      self.robots.append(robot)

  def localize_objectives(self, goal_names):
    self.final_goals = []
    for goal_name in goal_names:
      self.final_goals.append(find_goal_position(self.sim, self.clientID, name=goal_name))
    self.source_pos = find_source_position(self.sim, self.clientID)
    self.all_cubes = find_all_cube_positions(self.sim, self.clientID)

    for cube_pos in self.all_cubes:
      self.ax.scatter(cube_pos.position[0], cube_pos.position[1], marker="s", c="#cf2bb9", s=40)

    for goal_pos in self.final_goals:
      self.ax.scatter(goal_pos[0], goal_pos[1], marker="s", c="#d62020", s=50)

  def solve_routes(self):
    print("Calculating routes...")
    G = networkx.DiGraph()

    # Colocando os vértices vindo do início
    for i, cube_pos in enumerate(self.all_cubes):
      G.add_edge("Source", i, cost=dist3d(self.source_pos, cube_pos.position))

    # Colocando os vértices indo para o final
    for i, cube_pos in enumerate(self.all_cubes):
      G.add_edge(i, "Sink", cost=dist3d(self.final_goals[0], cube_pos.position))

    # Colocando os vértices entre todos os cubos
    for ia, cube_a in enumerate(self.all_cubes):
      for ib, cube_b in enumerate(self.all_cubes):
        if ia != ib:
          G.add_edge(ia, ib, cost=dist3d(cube_a.position, cube_b.position))

    n_stops = math.ceil(len(self.all_cubes) / len(self.robots))
    VRP = VehicleRoutingProblem(G, num_stops=n_stops, num_vehicles=len(self.robots), use_all_vehicles=True)
    VRP.solve()
    
    return [value[1 : -1] for value in VRP.best_routes.values()]

  def finalize(self):
    plt.savefig("robot_path.png")
    self.sim.simxStopSimulation(self.clientID, self.sim.simx_opmode_blocking)         
    self.sim.simxFinish(self.clientID)

  def assign_robots(self, target_pos):
    available_robots = list(range(len(self.robots)))
    assigned_robots = [-1 for _ in range(len(target_pos))]

    for i in range(len(target_pos)):
      best_robot_i = -1
      best_robot_dist = 99999
      for ai in available_robots:
        robot_pos, _ = self.robots[ai].get_position_orientation()
        robot_d = dist3d(target_pos[i], robot_pos)
        if robot_d < best_robot_dist:
          best_robot_dist = robot_d
          best_robot_i = ai
      assigned_robots[best_robot_i] = i
      available_robots.remove(best_robot_i)
    return assigned_robots

  def runner(self):
    robot_paths = self.solve_routes()
    robot_target_goal = [0 for _ in range(len(self.robots))]
    robots_done = [False for _ in range(len(self.robots))]

    first_cube_pos = [self.all_cubes[path[0]].position for path in robot_paths]
    assigned_robots = self.assign_robots(first_cube_pos)

    goal_assigned_robots = self.assign_robots(self.final_goals)

    print("Starting simulation...")
    try:
      t = 0
      startTime = time.time()
      lastTime = startTime

      while t < self.configs["runtime-limit"]:
        now = time.time()
        dt = now - lastTime

        for i in range(len(self.robots)):
          if robots_done[i]:
            pos, _ = self.robots[i].get_position_orientation()
            if dist3d(pos, self.robots[i].start_position) < 0.33:
              self.robots[i].stop()
            else:
              self.robots[i].tick(self.robots[i].start_position, backwards=True)
          else:
            goal_i = robot_target_goal[i]
            path_index = assigned_robots[i]
            if goal_i > len(robot_paths[path_index]): continue

            if goal_i < len(robot_paths[path_index]):
              target_cube_id = robot_paths[path_index][goal_i]
              target_goal_pos = self.all_cubes[target_cube_id].position
            else:
              target_goal_pos = self.final_goals[goal_assigned_robots[i]]

            pos, _ = self.robots[i].get_position_orientation()
            qgoal = np.array([target_goal_pos[0], target_goal_pos[1], np.deg2rad(90)])

            # Chegou no objetivo
            if dist3d(pos, target_goal_pos) < 1 and goal_i == len(robot_paths[path_index]):
              self.robots[i].max_v *= 3
              robots_done[i] = True
            else:
              self.robots[i].tick(qgoal)
              if dist3d(pos, target_goal_pos) < 1:
                robot_target_goal[i] += 1

        t = t + dt  
        lastTime = now
    except Exception as error:
      print("An exception occurred: {}".format(error))
    finally:
      self.finalize()