class SimCube:
  def __init__(self, handle, position):
    self.handle = handle
    self.position = position

  def __repr__(self):
    return str(self.position)

def find_all_cube_positions(sim, clientID, name="Objective"):
  all_cubes = []
  curr_ind = 0
  while True:
    _, cuboid_handle = sim.simxGetObjectHandle(clientID, f"{name}{curr_ind}", sim.simx_opmode_oneshot_wait)
    _, cuboid_pos = sim.simxGetObjectPosition(clientID, cuboid_handle, -1, sim.simx_opmode_oneshot_wait)
    if cuboid_handle == 0: break
    all_cubes.append(SimCube(cuboid_handle, cuboid_pos))
    curr_ind += 1
  return all_cubes

def find_goal_position(sim, clientID, name="Goal"):
  returnCode = 1
  while returnCode != 0:
    returnCode, goalHandle = sim.simxGetObjectHandle(clientID, name, sim.simx_opmode_oneshot_wait) 
    returnCode, final_goal = sim.simxGetObjectPosition(clientID, goalHandle, -1, sim.simx_opmode_oneshot_wait)
  return final_goal

def find_source_position(sim, clientID, name="Source"):
  returnCode = 1
  while returnCode != 0:
    returnCode, source_handle = sim.simxGetObjectHandle(clientID, name, sim.simx_opmode_oneshot_wait) 
    returnCode, source_pos = sim.simxGetObjectPosition(clientID, source_handle, -1, sim.simx_opmode_oneshot_wait)
  return source_pos