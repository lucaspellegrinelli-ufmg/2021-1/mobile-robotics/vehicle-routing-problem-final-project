import sim
from control.master_controller import MasterController

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

master = MasterController(sim, clientID, {
  "runtime-limit": 40
})

if clientID != -1:
  print("Connected to remote API server")

  robots = [ "Pioneer_p3dx_0", "Pioneer_p3dx_1",  "Pioneer_p3dx_2" ]
  goals = [ "Goal0", "Goal1", "Goal2" ]
  master.initialize_robots(robots)
  master.localize_objectives(goals)
  master.runner()
else:
  print ("Failed connecting to remote API server")
print ("Program ended")