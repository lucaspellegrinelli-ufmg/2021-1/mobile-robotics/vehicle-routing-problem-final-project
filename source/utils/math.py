import numpy as np

# Normalize angle to the range [-pi,pi)
def normalize_angle(angle):
  return np.mod(angle + np.pi, 2 * np.pi) - np.pi

def dist3d(c0, c1):
  return (c0[0] - c1[0]) ** 2 + (c0[1] - c1[1]) ** 2 + (c0[2] - c1[2]) ** 2